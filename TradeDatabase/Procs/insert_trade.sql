﻿CREATE PROCEDURE [dbo].[insert_trade]
	@sourceCurrency char(3),
	@destinationCurrency char(3),
	@lots float = 0,
	@price float
AS
	INSERT INTO TradeRecord (SourceCurrency, DestinationCurrency, Lots, Price) VALUES (@sourceCurrency, @destinationCurrency,@lots,@price)

