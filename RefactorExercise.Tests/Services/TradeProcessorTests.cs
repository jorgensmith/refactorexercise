using System.IO;
using System.Text;
using RefactorExercise.Services;
using Xunit;

namespace RefactorExercise.Tests.Services
{
    public class TradeProcessorTests
    {
        [Theory]
        [InlineData("fail")] // malformed file
        [InlineData("AUNO,123")] // incorrect number of fields
        [InlineData("AUNO,123,123")] // incorrect currency code
        [InlineData("AUDNOR,Foo,123")] // incorrect trade amount
        [InlineData("AUDNOR,123,Foo")] // incorrect trade price
        public void VerifyValidationRules(string inputData)
        {
            // Arrange
            var tradeProcessor = new TradeProcessor();

            // Act
            using (var test_Stream = new MemoryStream(Encoding.ASCII.GetBytes(inputData)))
            {
                tradeProcessor.ProcessTrades(test_Stream);
            }

            // Assert
            // at this time without jumping through hoops, due to the how the existing code runs,
            // I can only observe this manually using NCrunch's coverage (green vs grey bars) -
            // with the Integration test below commented out.
        }

        [Theory]
        [InlineData("AUDNOR,123,123\nAUDNOR,124,124")] // Valid multi line transaction
        public void IntegrationVerifyValidProcessing(string inputData)
        {
            // Arrange
            var tradeProcessor = new TradeProcessor();

            // Act
            using (var test_Stream = new MemoryStream(Encoding.ASCII.GetBytes(inputData)))
            {
                tradeProcessor.ProcessTrades(test_Stream);
            }

            // Assert
            // at this time without jumping through hoops, due to the how the existing code runs 
            // I can only observe this manually using NCrunch's coverage (green vs grey bars)
        }
    }
}