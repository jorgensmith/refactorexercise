﻿using System;
using System.IO;
using RefactorExercise.Services;

namespace RefactorExercise
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Input file expected. Aborting.");
                return;
            }

            var processor = new TradeProcessor();
            using (FileStream stream = File.Open(args[0], FileMode.Open))
            {
                processor.ProcessTrades(stream);
            }
        }
    }
}