﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace RefactorExercise.Services
{
    public class TradeProcessor
    {
        private static readonly float LotSize = 100000f;

        public void ProcessTrades(Stream stream)
        {
            var lines = new List<string>();
            using (var reader = new StreamReader(stream))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    lines.Add(line);
            }

            var trades = new List<TradeRecord>();
            var lineCount = 1;
            foreach (string line in lines)
            {
                string[] fields = line.Split(new[] {','});

                if (fields.Length != 3)
                {
                    Console.WriteLine($"WARN: Line {lineCount} malformed. Only {fields.Length} field(s) found.");
                    continue;
                }

                if (fields[0].Length != 6)
                {
                    Console.WriteLine($"WARN: Trade currencies on line {0} malformed: [{fields[0]}]");
                    continue;
                }

                int tradeAmount;
                if (!int.TryParse(fields[1], out tradeAmount))
                {
                    Console.WriteLine($"WARN: Trade amount on line {0} is not a valid integer: [{fields[1]}]");
                    continue;
                }

                decimal tradePrice;
                if (!decimal.TryParse(fields[2], out tradePrice))
                {
                    Console.WriteLine($"WARN: Trade price on line {0} is not a valid decimal: [{fields[1]}]");
                    continue;
                }

                string sourceCurrencyCode = fields[0].Substring(0, 3);
                string destinationCurrencyCode = fields[0].Substring(3, 3);

                // Calculate values
                var trade = new TradeRecord
                {
                    SourceCurrency = sourceCurrencyCode,
                    DestinationCurrency = destinationCurrencyCode,
                    Lots = tradeAmount / LotSize,
                    Price = tradePrice
                };

                trades.Add(trade);

                lineCount++;
            }
            using (var connection = new SqlConnection(
                @"Data Source=(localdb)\ProjectsV13;Initial Catalog=TradeDatabase;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True")
            )
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    foreach (TradeRecord trade in trades)
                    {
                        SqlCommand command = connection.CreateCommand();
                        command.Transaction = transaction;
                        command.CommandType = CommandType.StoredProcedure;
                        command.CommandText = "dbo.insert_trade";
                        command.Parameters.AddWithValue("@sourceCurrency", trade.SourceCurrency);
                        command.Parameters.AddWithValue("@destinationCurrency", trade.DestinationCurrency);
                        command.Parameters.AddWithValue("@lots", trade.Lots);
                        command.Parameters.AddWithValue("@price", trade.Price);

                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                }
                connection.Close();
            }

            Console.WriteLine($"INFO: {trades.Count} trades processed");
        }
    }

    public class TradeRecord
    {
        public string SourceCurrency { get; set; }
        public string DestinationCurrency { get; set; }
        public float Lots { get; set; }
        public decimal Price { get; set; }
    }
}