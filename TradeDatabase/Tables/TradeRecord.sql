﻿CREATE TABLE [dbo].[TradeRecord]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[SourceCurrency] CHAR(3) NOT NULL,
	[DestinationCurrency] CHAR(3) NOT NULL,
	[Lots] REAL NOT NULL, 
    [Price] DECIMAL(19, 5) NOT NULL, 
    [TradeDate] datetime NOT NULL DEFAULT GETDATE()

)

